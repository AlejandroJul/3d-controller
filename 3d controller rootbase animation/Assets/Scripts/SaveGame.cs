﻿using Mono.Data.SqliteClient;
using System.IO;
using UnityEngine;

public class SaveGame : MonoBehaviour
{
    string path = @"D:\Github y clase\3d-Controller\3d controller rootbase animation\Assets\Scripts\SaveGame.json";

    void Start()
    {
        //Deserializar
        try
        {
            PlayerSettings player = JsonUtility.FromJson<PlayerSettings>(MenuManager.currentGame.positionData);
            transform.position = player.position;
        }
        catch
        {
            print("esta mal");
        }

    }

    void Update()
    {
        //Serializar


        PlayerSettings playerSave = new PlayerSettings()
        {
            position = transform.position
        };

        File.WriteAllText(path, JsonUtility.ToJson(playerSave));

    }

    private void OnApplicationQuit()
    {

        //Serializar
        PlayerSettings playerSave = new PlayerSettings()
        {
            position = transform.position
        };
        string playerData = JsonUtility.ToJson(playerSave);
        SavePlayerData(MenuManager.currentGame.gameID, playerData);

    }

    public void SavePlayerData(int gameID,string savePlayerData)
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/PartidasGuardadas.db");
        conexion.Open();

        //  string creacion = "SELECT * FROM Partidas WHERE CodPartida = " + gameID;
        // SqliteCommand cmd = new SqliteCommand(creacion, conexion);
        // cmd.ExecuteNonQuery();


        string mostrar = "UPDATE Partidas SET Posicion = '" + savePlayerData + "' WHERE CodPartida = " + gameID;
        SqliteCommand cmd = new SqliteCommand(mostrar, conexion);
        SqliteDataReader datos = cmd.ExecuteReader();

        
        

        conexion.Close();
      
    }
}


[System.Serializable]
class PlayerSettings
{
    public Vector3 position;
}
