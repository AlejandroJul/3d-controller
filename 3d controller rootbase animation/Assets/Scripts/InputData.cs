﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public struct InputData 
{
    //basic movement
    public float hmovement;
    public float vmovement;
    //mouse rotation
    public float verticalmouse;
    public float horizontalmouse;
    //extra movement
    public bool Jump;
    public bool dash;
    public bool atack;

    public void getInput()
    {
        hmovement = Input.GetAxis("Horizontal");
        vmovement = Input.GetAxis("Vertical");

        verticalmouse = Input.GetAxis("Mouse X");
        horizontalmouse = Input.GetAxis("Mouse Y");

        dash = Input.GetButton("Dash");
        Jump = Input.GetButtonDown("Jump");
        atack = Input.GetButtonDown("atack");
    }
}