﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData 
{
    public string scoreData;
    public string positionData;
    public int gameID;
}
