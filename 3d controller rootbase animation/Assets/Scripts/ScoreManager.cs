﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Mono.Data.SqliteClient;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public TextMeshProUGUI Text;
    public ScoreData score;

    void Start()
    {
        if (instance == null)
        {
            try 
            {
                score =   JsonUtility.FromJson<ScoreData>(MenuManager.currentGame.scoreData);
            }
            catch
            {
                score = new ScoreData();
            }
            instance = this;
            if (score == null)
            {
                score = new ScoreData();
            }
        }
        Text.text = "X" + score.score.ToString();
    } 

    // Update is called once per frame
   public void ChangeScore(int coinValue)
    {
        score.score += coinValue;
        Text.text = "X" + score.score.ToString();
    }

    private void OnApplicationQuit()
    {

        
        
        string playerData = JsonUtility.ToJson(score);
        SavePlayerData(MenuManager.currentGame.gameID, playerData);

    }

    public void SavePlayerData(int gameID, string savePlayerData)
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/PartidasGuardadas.db");
        conexion.Open();

        string mostrar = "UPDATE Partidas SET Puntuacion = '" + savePlayerData + "' WHERE CodPartida = " + gameID;
        SqliteCommand cmd = new SqliteCommand(mostrar, conexion);
        SqliteDataReader datos = cmd.ExecuteReader();




        conexion.Close();

    }
}
[System.Serializable]
public class ScoreData
{
    public int score = 0;


}
