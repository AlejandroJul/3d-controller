﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;
    public UnityEvent onInteractionInput;
    private InputData input;
    private CharacterAnimBasedMovement characterMovement;

    public bool onInteractionZone { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
        
    }

    // Update is called once per frame
    void Update()
    {
        input.getInput();

        if (onInteractionZone && input.Jump)
        {
            onInteractionInput.Invoke();
        }
        else
        characterMovement.moveCharacter(input.hmovement, input.vmovement, cam, input.Jump, input.dash, input.atack);
    }
}
