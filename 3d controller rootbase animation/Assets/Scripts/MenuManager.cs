﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.SqliteClient;
using System;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public static GameData currentGame;


    public void LoadGameData(int gameID)
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/PartidasGuardadas.db");
        conexion.Open();

       //  string creacion = "SELECT * FROM Partidas WHERE CodPartida = " + gameID;
       // SqliteCommand cmd = new SqliteCommand(creacion, conexion);
       // cmd.ExecuteNonQuery();

        
        string mostrar = "SELECT * FROM Partidas WHERE CodPartida = " + gameID;
        SqliteCommand cmd = new SqliteCommand(mostrar, conexion);
        SqliteDataReader datos = cmd.ExecuteReader();

        GameData gameData = new GameData();

        while (datos.Read())
        {
            gameData.gameID = datos.GetInt32(0);
            gameData.scoreData = datos.GetString(1);
            gameData.positionData = datos.GetString(2);
        }
        currentGame = gameData;

        conexion.Close();

        SceneManager.LoadScene("Juego");

    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
